require 'spec_helper'

describe 'apt-zabbix::default' do
	#context 'Creates Zabbix APT repository' do
	#	let(:chef_run) do
	#		ChefSpec::SoloRunner.new do |node|
	#			# Create a new environment (you could also use a different :let block or :before block)
	#			env = Chef::Environment.new
	#			env.name 'unit_test'
    #
	#			# Stub the node to return this environment
	#			allow(node).to receive(:chef_environment).and_return(env.name)
    #
	#			# Stub any calls to Environment.load to return this environment
	#			allow(Chef::Environment).to receive(:load).and_return(env)
    #
	#			# Stubbing out fqdn node attribute
	#			node.automatic['fqdn'] = 'unit.testing.stub'
	#		end.converge(described_recipe)
	#	end
    virtual_box_version = '5.0'

    let(:runner) { ChefSpec::ServerRunner.new(platform: 'debian', version: '8.6') }
    let(:chef_run) { runner.converge('apt-zabbix::default') }
    #let(:chef_run) do
    #    ChefSpec::SoloRunner.new do |node|
    #        node.set['virtualbox']['version'] = virtual_box_version
    #    end.converge(described_recipe)
    #end

    #before do
    #    allow(::File).to receive(:executable?).and_return(true)
    #end

	it 'installs the apt repository' do
        expect(chef_run).to add_apt_repository('zabbix').with(
            :uri => 'http://repo.zabbix.com/zabbix/3.0/debian',
            :key => 'zabbix-official-repo.key',
            :distribution => 'jessie',
            :components => ['main']
        )
		#expect(chef_run).to render_file('/etc/apt/sources.list.d/zabbix.list').with_content('deb      "http://repo.zabbix.com/zabbix/3.0/debian" jessie main')
	end
end

