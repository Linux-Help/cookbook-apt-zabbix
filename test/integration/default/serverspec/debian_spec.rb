require 'spec_helper'

describe 'apt-zabbix::default' do
    ### REPOSITORIES
    #
    describe 'Repositories should be installed:' do
        describe file('/etc/apt/sources.list.d') do
            it { should be_directory }
            it { should be_mode 755 }
            it { should be_owned_by 'root' }
            it { should be_grouped_into 'root' }
        end

        describe file('/etc/apt/sources.list.d/zabbix.list') do
            it { should exist }
            it { should be_mode 644 }
            it { should be_owned_by 'root' }
            it { should be_grouped_into 'root' }
            its(:content) { should match('deb      "http://repo.zabbix.com/zabbix/3.0/debian" jessie main') }
        end
    end
end

